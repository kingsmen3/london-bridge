package tatooine.mosquitto;

import com.google.api.core.ApiFuture;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import org.eclipse.paho.client.mqttv3.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

public class MosquittoToPubSubBridge {

    private static Publisher publisher = null;
    private static final String projectId = "local-tempo-407113";
    private static final String topicId = "commands";

    //private static final String credentialsPath = "src/main/resources/arthur.json";
    private static final InputStream credentialsPath = MosquittoToPubSubBridge.class.getClassLoader().getResourceAsStream("arthur.json");
    public static void main(String[] args) throws MqttException, IOException, InterruptedException {

        // Charger la clé du compte de service à partir d'un fichier
        GoogleCredentials credentials = ServiceAccountCredentials.fromStream(credentialsPath)
                .createScoped(Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));

        // Initialiser le publisher Pub/Sub avec les informations d'identification
        ProjectTopicName topicName = ProjectTopicName.of(projectId, topicId);
        publisher = Publisher.newBuilder(topicName).setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();

        String mqttBroker = "tcp://172.31.252.134:1801";
        String mqttTopic = "weather_topic";

        MqttClient mqttClient = new MqttClient(mqttBroker, MqttClient.generateClientId());
        mqttClient.connect();
        mqttClient.subscribe(mqttTopic, (topic, message) -> {

            String messagePayload = new String(message.getPayload());
            System.out.println("Received from Mosquitto: " + messagePayload);

            // Publish message to Google Cloud Pub/Sub
            publishToPubSub(messagePayload);
            ProjectTopicName topicName2 =ProjectTopicName.of(projectId, "weather-data-queue-real-time");
            publisher = Publisher.newBuilder(topicName2).setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
            publishToPubSub(messagePayload);
        });

        // Keep the program running to listen for MQTT messages
        synchronized (MosquittoToPubSubBridge.class) {
            MosquittoToPubSubBridge.class.wait();
        }
    }

    private static void publishToPubSub(String messagePayload) {
        ByteString data = ByteString.copyFromUtf8(messagePayload);
        PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

        try {
            // Once published, returns a server-assigned message ID (unique within the topic)
            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
            String messageId = messageIdFuture.get();
            System.out.println("Published message ID: " + messageId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}